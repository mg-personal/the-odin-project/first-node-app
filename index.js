//Node and Express setup
const express = require("express");
const app = express();
var fs = require("fs");

//get routes
let index = fs.readFileSync("index.html");
let about = fs.readFileSync("about.html");
let contactme = fs.readFileSync("contact-me.html");
let errorpage = fs.readFileSync("404.html");

// home page route
app.get("/", function(req, res) {
  res.redirect("/index");
});

// index Page route
app.get("/index", function(req, res) {
  res.end(index);
});

// about page route
app.get("/about", function(req, res) {
  res.end(about);
});

// contact route
app.get("/contact-me", function(req, res) {
  res.end(contactme);
});

// Error route
app.get("/*", function(req, res) {
  res.end(errorpage);
});

app.listen(3000, function() {
  console.log("listening on port 3000");
});
